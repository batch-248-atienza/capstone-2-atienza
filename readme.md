# Turo-Turo Treats

## Project Description
The following is a REST API using MERN stack developed as Capstone 2 requirement under Zuitt Coding Bootcamp. The API was developed as a backend for an e-commerce site.

## Technologies Used
The following npm packages were used for the development of the API:

- Bcrypt: used to implement password hasing during user authentication
- Cors: middleware that allows cross-origin resource sharing
- Express: a nodeJS framework that helps in managing servers and route
- JSONWebToken: a compact and self-contained way for securely transmitting information
- Mongoose: allows the API to connect with the MongoDB database
- Nodemon: improves quality of life during development by automatically restarting the node server once changes are detected

## Features
The following are the features of the developed API:

- [x] Checking of Existing Email
- [x] User Registration and Authentication
- [x] Create New Products (Admin Only)
- [x] Update Product Information (Admin Only)
- [x] Retrieve all Products (Admin Only)
- [x] Archive and Reactivate Products (Admin Only)
- [x] Set User as Admin (Admin Only)
- [x] Retrieve all Orders (Admin Only)
- [x] Retrieve Single Product 
- [x] Retrieve User Details
- [x] Retrieve User Orders
- [x] Add Products to Cart
- [x] Update Product Quantities in Cart
- [x] Remove Products from Cart
- [x] Get Subtotal for each Item in Cart
- [x] Total Price for all Items in Cart
- [x] User Checkout 
- [x] Retrieve All Active Products

## Login Details
Two user login details are provided for the testing of this API:

- Regular User
	- Email: user@mail.com
	- Password: user1234

- Admin User
	- Email: admin@mail.com
	- Password: admin1234

## API Responses
Responses of this API have been standardized for all actions. All actions will result in two responses from the API, a status response and a message response.

The status response will consist of either a true or false, indicating if the requested action was done successfully. For example, a status of true on register user means that the user was successfully registered. And a status of false on create order (checkout) function indicates that the checkout was not successful, one possible reason is perhaps there is nothing to checkout.

The message response will consist of additional information regarding the action. When requesting data from the API such as a list of users or products, the data will be sent in the message response. The message response may also include descriptive sentences such as 'Operation failed. Product does not exist.' or 'Operation failed. Product already added to database.'

## Routes

### Users (/users)
The following is a description of the user routes:

#### Check Email (/checkEmailExists)
A http POST request that checks if a specific email is already registered in the database.

INPUT
```ruby
{
	"email": ${email}
}
```

OUTPUT
If the email is already registered, the API will return a status of true as well as a message indicating that it is registered. Otherwise, it will return a status of false and a message indicating that the email does not exist.

#### Register (/register)
A http POST request that registers a user into the database.

INPUT
```ruby
{
	"firstName": ${firstName},
	"lastName": ${lastName},
	"email": ${email},
	"mobileNo": ${mobileNo},
	"password": ${password}
}
```

OUTPUT
If registration is successful, the API will return a status of true and a message containing the details sent except for the password. Otherwise if the registration was unsuccessful, the API will return a status of false as well as a message indicating if the email is already existing.

#### Login (/login)
A http POST request that logs in the user.

INPUT
```ruby
{
	"email": ${email},
	"password": ${password}
}
```

OUTPUT
If login is successful, the API will return a status of true and a message containing a generated access token. Otherwise, the API will return a status of false as well as a message indicating if no such user was found or if the provided password was incorrect.

#### Update Admin (/updateAdmin/:userId)
A http POST request that change's the spicified user's status into an admin.

Note: To use this function, access token of an admin must be sent to the API.

INPUT
This route does not take any inputs except for the access token.

OUTPUT
If the process is successful, the API will return a status of true and a message indicating that the user type change into admin was successful. Otherwise, the API will return a status of false as well as a message indicating if either no user was found or the selected user is already an admin.

#### Get Profile (/getUserDetails)
A http GET request that returns details of the logged-in user.

Note: To use this function, access token of any type must be sent to the API.

INPUT
This route does not take any inputs except for the access token.

OUTPUT
If the process is successful, the API will return a status of true and a message containing the retrieved user details except for the password. Otherwise if an error is encountered, the API will return a status of false as well as the error message encountered.

#### Get Cart (/cart)
A http GET request that returns the logged-in user's cart.

Note: To use this function, access token of type user must be sent to the API.

INPUT
This route does not take any inputs except for the access token.

OUTPUT
If the retrieval of the user's cart is successful, the API will return a status of true and a message containing an object with the contents of the user's cart as well as the subtotal per product and the total amount of the entire cart. Otherwise, the API will return a status of false and a message specifying if any errors were encountered or if the user's cart is currently empty.

#### Checkout (/checkout)
A http POST request that checkouts the entire contents of the logged-in user's cart.

Note: To use this function, access token of type user must be sent to the API.

INPUT
This route does not take any inputs except for the acces token.

OUTPUT
If the checkout is successful, the API will return a status of true and a message specifying that the order was successfully placed. Otherwise, the API will return a status of false and a message specifying if any errors were encountered or if there are no items to checkout.