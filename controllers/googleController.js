//Import the model schema and authenticator
const User = require("../models/User");
const auth = require("../auth");

module.exports.login = (req, res)=>{
    console.log("Received request")
    const userData = auth.googleDecode(req.body.data.credential);
    console.log(userData);
    //check if email is already registered
    User.findOne({email: userData.email})
	.then(result=>{
		//If email was not found
		if(result === null){
			//placeholder since pw and mobile is required as per model schema
			const hashedPw = 1
            const mobile = 09123456789

			//Define the new user
			let newUser = new User({
				firstName: userData.given_name,
				lastName: userData.family_name,
				email: userData.email,
				password: hashedPw,
                mobileNo: mobile
			});
			//Save the new user into the database
			newUser.save()
			.then(result=>{
				result.password = "";
				res.send({status: true, message: auth.createAccessToken(result)})
			})
			//Catch any errors
			.catch(error=>res.send({status: false, message: `An error occured during saving to database. ${error}`}));
			//Log a message to the console
			console.log(`New user successfully registered.`);
		//If an email was found, return the JWT token to proceed with login
		}else{
			return res.send({status: true, message: auth.createAccessToken(result)});
		};
	})
    .catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));
};