//Initialize package dependencies
const bcrypt = require("bcrypt");

//Import the model schema and authenticator
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const auth = require("../auth");

//Check if the email already exists
module.exports.checkIfEmailExists = (req, res)=>{
	//Log a message to the console
	console.log(`Checking email: ${req.body.email}`);
	//Check if email is already registered
	User.findOne({email: req.body.email})
	.then(result=>{
		//If email was not found
		if(result === null){
			//Log a message to the console
			console.log(`Email does not exist`);
			return res.send({status: false, message: "Email not found."});
		//If an email was found
		}else{
			//Log a message to the console
			console.log(`Email already exists`);
			return res.send({status: true, message: "Email is already registered."});
		};
	})
	//Catch any errors
	.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));
};

//Register a new user
module.exports.registerUser = (req, res)=>{
	//Log a message to the console
	console.log(`Attempting to register a new user`);
	//Check first if the email is already registered (similar to checkIfEmailExists)
	User.findOne({email: req.body.email})
	.then(result=>{
		//If email was not found
		if(result === null){
			//Hash the password
			const hashedPw = bcrypt.hashSync(req.body.password, 10);

			//Define the new user
			let newUser = new User({
				firstName: req.body.firstName,
				lastName: req.body.lastName,
				email: req.body.email,
				password: hashedPw,
				mobileNo: req.body.mobileNo
			});
			//Save the new user into the database
			newUser.save()
			.then(result=>{
				result.password = "";
				res.send({status: true, message: result})
			})
			//Catch any errors
			.catch(error=>res.send({status: false, message: `An error occured during saving to database. ${error}`}));
			//Log a message to the console
			console.log(`New user successfully registered.`);
		//If an email was found
		}else{
			//Log a message to the console
			console.log(`Registration failed, existing email found`);
			return res.send({status: false, message: "Registration failed. Email already in use."});
		};
	})
	//Catch any errors
	.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));
};

//Login the user
module.exports.loginUser = (req, res)=>{
	//Log a message to the console
	console.log(`Attempting to login ${req.body.email}`);
	//Find the user in the db using the email
	User.findOne({email: req.body.email})
	.then(result=>{
		//if no user was found
		if(result === null){
			//Log a message to the console
			console.log(`No such user found.`);
			return res.send({status: false, message: `No such user found.`});
		}else{
			//Log a message to the console
			console.log(`User found. Comparing hashed passwords`);
			//compare hashed passwords
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
			if(isPasswordCorrect){
				//Log a message to the console
				console.log(`Creating access token`);
				return res.send({status: true, message: auth.createAccessToken(result)});
			}else{
				//Log a message to the console
				console.log(`Hased passwords did not match.`);
				return res.send({status: false, message: `Incorrect password.`});
			};
		};
	})
	//Catch any errors
	.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));	
};

//Make another user an admin
module.exports.makeAdmin = (req, res)=>{
	//Log a message to the console
	console.log(`Attempting to change user status into admin`);
	//Find the user in the db using id
	User.findOne({_id: req.params.userId})
	.then(result=>{
		if(result === null){
			//Log a message to the console
			console.log(`No such user found.`);
			return res.send({status: false, message: `No such user found.`})
		}else{
			//Check if user is already an admin or not
			if(result.isAdmin == true){
				//Log a message to the console
				console.log(`User is already an admin!`);
				res.send({status: false, message: `User is already an admin!`})
			}else{
				result.isAdmin = true;
				result.save()
				.then(result=>res.send({status: true, message: "Admin change successful."}))
				//Catch any errors
				.catch(error=>res.send({status: false, message: `An error occured during saving to database. ${error}`}));
				//Log a message to the console
				console.log(`User admin status changed successfully`);
			};
		};
	})
	//Catch any errors
	.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));
};

//Remove another user as an admin
module.exports.removeAdmin = (req, res)=>{
	//Log a message to the console
	console.log(`Attempting to change user status into admin`);
	//Find the user in the db using id
	User.findOne({_id: req.params.userId})
	.then(result=>{
		if(result === null){
			//Log a message to the console
			console.log(`No such user found.`);
			return res.send({status: false, message: `No such user found.`})
		}else{
			//Check if user is already an admin or not
			if(result.isAdmin == false){
				//Log a message to the console
				console.log(`User is not an admin!`);
				res.send({status: false, message: `User is nor an admin!`})
			}else{
				result.isAdmin = false;
				result.save()
				.then(result=>res.send({status: true, message: "Admin change successful."}))
				//Catch any errors
				.catch(error=>res.send({status: false, message: `An error occured during saving to database. ${error}`}));
				//Log a message to the console
				console.log(`User admin status changed successfully`);
			};
		};
	})
	//Catch any errors
	.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));
};

//Return user details
module.exports.getProfile = (req, res)=>{
	//Log a message to the console
	console.log(`Attempting to get user details`);
	//Decode token
	const userData = auth.decode(req.headers.authorization);
	//Find the user in the db using id
	User.findById(userData.id)
	.then(result=>{
		result.password = "";
		return res.send({status: true, message: result});
	})
	//Catch any errors
	.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));
};

//Create an order
//Checking out items and creating an order only applicable to items in the cart
//As of now, functionality is to checkout ALL items in the cart
//Future improvement is to add functionality of only checking out select items in the cart
module.exports.createOrder = async (req, res)=>{
	//Log a message to the console
	console.log(`Attempting to checkout items in cart`);
	//Decode token
	const userData = auth.decode(req.headers.authorization);

	//Obtain the productIds of the user's cart
	let cart = await User.findById(userData.id)
	.then(result=>result.cart)
	.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));

	//Check if cart has contents
	if(cart.length>0){
		//Extract productIds and quantities of the items to be checked out
		const itemsToCheckout = req.body.productIds;
		const productData = [];

		//Loop thorugh each item of the cart
		for(const item of cart){
			if(itemsToCheckout.includes(item.productId)){
				const product = await Product.findById(item.productId)
				.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));

				const itemPrice = product.price;
				const data = {
	            	productId: item.productId,
	            	quantity: item.quantity,
	            	price: itemPrice,
	            	totalAmount: item.quantity * itemPrice
	        	};
	        productData.push(data);
			};
		};

		if(productData.length === 0){
			return res.send({status: false, message: `No items in cart.`});
		};

		let totalAmount = req.body.totalAmount;
		let shippingFee = req.body.shippingFee;
		let deliveryAddress = req.body.deliveryAddress;

		//Add a new order
		let newOrder = new Order({
			userId: userData.id,
			totalAmount: totalAmount,
			shippingFee: shippingFee,
			deliveryAddress: deliveryAddress
		});
		for(const product of productData){
			newOrder.products.push({productId: product.productId,quantity: product.quantity, price: product.price});
		};
		//Save the new order into the database
		await newOrder.save()
		//Catch any errors
		.catch(error=>res.send({status: false, message: `An error occured during saving to database. ${error}`}));
		//Get the new order's id
		const newOrderId = newOrder._id;

		//Update the users database and remove the checked out items
		cart = cart.filter((item) => !itemsToCheckout.includes(item.productId));
		await User.findById(userData.id)
		.then(result=>{
			result.cart = cart;
			result.save()
			//Catch any errors
			.catch(error=>res.send({status: false, message: `An error occured during saving to database. ${error}`}));
		})
		.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));

		//Update the products database
		for(const item of productData){
			await Product.findById(item.productId)
			.then(result=>{
				const data = {
				    orderId: newOrderId,
				    quantity: item.quantity
				};
				result.orders.push(data);
				result.save()
				//Catch any errors
				.catch(error=>res.send({status: false, message: `An error occured during saving to database. ${error}`}));
			})
			.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));
		};

		return res.send({status: true, message: `Order successfully placed`});
	}else{
		return res.send({status: false, message: `Cart is empty, nothing to checkout.`});
	};
};

//Return the user's cart
module.exports.getCart = async (req, res)=>{
	//Log a message to the console
	console.log(`Attempting to return the user's cart`);
	//Decode token
	const userData = auth.decode(req.headers.authorization);

	//Obtain the productIds of the user's cart
	const cart = await User.findById(userData.id)
	.then(result=>result.cart)
	.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));

	//Check if cart has contents
	if(cart.length>0){
		//Extract productIds and quantities
		const productData = [];

		//Loop thorugh each item of the cart
		for(const item of cart){
			const product = await Product.findById(item.productId)
			.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));

			const itemPrice = product.price;
			const data = {
	            productId: item.productId,
	            quantity: item.quantity,
	            price: itemPrice,
	            totalAmount: item.quantity * itemPrice
	        };
	        productData.push(data);
		};

		//Compute the total amount of the order
		let totalAmount = 0;
		for(const data of productData){
			totalAmount += data.totalAmount;
		};

		return res.send({status: true, message: {cart: productData, totalAmount: totalAmount}});
	}else{
		return res.send({status: false, message: `Cart is empty`});
	};
};

//Get All Users
module.exports.returnAllUsers = (req, res)=>{
	//Log a message to the console
	console.log(`Attempting to retrieve all users`);
	User.find({})
	.then(result=>res.send({status: true, message: result}))
	//Catch any errors
	.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));
};