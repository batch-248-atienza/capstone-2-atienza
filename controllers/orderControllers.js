//Initialize package dependencies
const bcrypt = require("bcrypt");

//Import the model schema and authenticator
const Order = require("../models/Order");
const auth = require("../auth");

//Get All Orders
module.exports.returnAllOrders = (req, res)=>{
	//Log a message to the console
	console.log(`Attempting to retrieve all orders`);
	//Check first if the product already exists
	Order.find({})
	.then(result=>res.send({status: true, message: result}))
	//Catch any errors
	.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));
};

//Get User Orders
module.exports.getUserOrders = (req, res)=>{
	//Log a message to the console
	console.log(`Attempting to retrieve user orders`);
	//Decode token
	const userData = auth.decode(req.headers.authorization);
	//Find the user in the db of orders
	Order.find({userId: userData.id})
	.then(result=>res.send({status: true, message: result}))
	//Catch any errors
	.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));
};