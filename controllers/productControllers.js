//Initialize package dependencies
const bcrypt = require("bcrypt");

//Import the model schema and authenticator
const Product = require("../models/Product");
const User = require("../models/User");
const auth = require("../auth");

//Create a product
module.exports.createProduct = (req, res)=>{
	//Log a message to the console
	console.log(`Attempting to add a new product`);
	//Check first if the product already exists
	Product.findOne({name: req.body.name})
	.then(result=>{
		//If product is not yet existing
		if(result === null){
			//Define the new product
			let newProduct = new Product({
				name: req.body.name,
				description: req.body.description,
				price: req.body.price
			});
			//Save the new product into the database
			newProduct.save()
			.then(result=>res.send({status: true, message: result}))
			//Catch any errors
			.catch(error=>res.send({status: false, message: `An error occured during saving to database. ${error}`}));
			//Log a message to the console
			console.log(`New product successfully added.`);
		//If a product was found
		}else{
			//Log a message to the console
			console.log(`Operation failed, existing product found`);
			return res.send({status: false, message: "Operation failed. Product already added to database."});
		};
	})
	//Catch any errors
	.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));
};

//Return all active products
module.exports.getAllActiveProducts = (req, res)=>{
	//Log a message to the console
	console.log(`Attempting to retrieve all active products`);
	Product.find({isActive: true})
	.then(result=>res.send({status: true, message: result}))
	//Catch any errors
	.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));
};

//Return all products
module.exports.getAllProducts = (req, res)=>{
	//Log a message to the console
	console.log(`Attempting to retrieve all products`);
	Product.find({})
	.then(result=>res.send({status: true, message: result}))
	//Catch any errors
	.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));
};

//Return a specific products
module.exports.getProduct = (req, res)=>{
	//Log a message to the console
	console.log(`Attempting to find the product to return`);
	//Attempt to find the product
	Product.findOne({_id: req.params.productId})
	.then(result=>{
		//If product is not existing
		if(result === null){
			//Log a message to the console
			console.log(`Operation failed, product not found`);
			return res.send({status: false, message: "Operation failed. Product does not exist."});
		//If a product was found
		}else{
			//Log a message to the console
			console.log(`Operation success, existing product found`);
			return res.send({status: true, message: result});
		};
	})
	//Catch any errors
	.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));
};

//Update a product
module.exports.updateProduct = (req, res)=>{
	//Define the updated product
	let updatedProduct = {
		photo: req.body.photo,
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	};
	//Log a message to the console
	console.log(`Attempting to find the product for update`);
	//Attempt to find and update the product
	Product.findByIdAndUpdate(req.params.productId, updatedProduct)
	.then(result=>{
		//If product is not existing
		if(result === null){
			//Log a message to the console
			console.log(`Operation failed, product not found`);
			return res.send({status: false, message: "Operation failed. Product does not exist."});
		//If a product was found
		}else{
			//Log a message to the console
			console.log(`Operation success, existing product updated`);
			return res.send({status: true, message: `Product ${result.name} with ID ${result._id} updated successfully.`});
		};
	})
	//Catch any errors
	.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));
};

//Archive a product
module.exports.archiveProduct = (req, res)=>{
	//Log a message to the console
	console.log(`Attempting to find the product to archive`);
	//Attempt to find the product
	Product.findOne({_id: req.params.productId})
	.then(result=>{
		//If product is not existing
		if(result === null){
			//Log a message to the console
			console.log(`Operation failed, product not found`);
			return res.send({status: false, message: "Operation failed. Product does not exist."});
		//If a product was found
		}else{
			result.isActive = false;
			result.save()
			.then(result=>res.send({status: true, message: `Product ${result.name} with ID ${result._id} archived successfully.`}))
			//Catch any errors
			.catch(error=>res.send({status: false, message: `An error occured during saving to database. ${error}`}));
			//Log a message to the console
			console.log(`Product successfully archived.`);
		};
	})
	//Catch any errors
	.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));
};

//Activate a product
module.exports.activateProduct = (req, res)=>{
	//Log a message to the console
	console.log(`Attempting to find the product to activate`);
	//Attempt to find the product
	Product.findOne({_id: req.params.productId})
	.then(result=>{
		//If product is not existing
		if(result === null){
			//Log a message to the console
			console.log(`Operation failed, product not found`);
			return res.send({status: false, message: "Operation failed. Product does not exist."});
		//If a product was found
		}else{
			result.isActive = true;
			result.save()
			.then(result=>res.send({status: true, message: `Product ${result.name} with ID ${result._id} activated successfully.`}))
			//Catch any errors
			.catch(error=>res.send({status: false, message: `An error occured during saving to database. ${error}`}));
			//Log a message to the console
			console.log(`Product successfully archived.`);
		};
	})
	//Catch any errors
	.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));
};

//Add product to cart
module.exports.addToCart = (req, res)=>{
	//Log a message to the console
	console.log(`Attempting to add product to cart`);
	//Decode token
	const userData = auth.decode(req.headers.authorization);

	//There needs to be a quantity provided
	if(req.body.quantity>0){
		//Update User model
		User.findById(userData.id)
		.then(user=>{
			//Check first if item is already in cart, if true simply update quantity
			const indexInCart = user.cart.findIndex(item=>item.productId === req.params.productId);
			if(indexInCart >= 0){
				user.cart[indexInCart].quantity += req.body.quantity;
			}else{
				user.cart.push({productId:req.params.productId, quantity:req.body.quantity});
			};
			return user.save()
			.then((result, error)=>{
				if(error){
					return res.send({status: false, message: `An error occured during saving to database. ${error}`});
				}else{
					//Log a message to the console
				console.log(`Product successfully added to cart.`);
					return res.send({status: true, message: `Product added to cart successfully.`});
				};
			});
		})
		//Catch any errors
		.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));
	}else{
		return res.send({status: false, message: `Quantity was not specified.`});
	};
};

//Edit the product in cart
module.exports.editCart = (req, res)=>{
	//Log a message to the console
	console.log(`Attempting to edit product in cart`);
	//Decode token
	const userData = auth.decode(req.headers.authorization);

	//There needs to be a quantity provided
	if(req.body.quantity>0){
		//Update User model
		User.findById(userData.id)
		.then(user=>{
			//Check if item is in cart
			const indexInCart = user.cart.findIndex(item=>item.productId === req.params.productId);
			if(indexInCart >= 0){
				user.cart[indexInCart].quantity = req.body.quantity;
				return user.save()
				.then((result, error)=>{
					if(error){
						return res.send({status: false, message: `An error occured during saving to database. ${error}`});
					}else{
						//Log a message to the console
					console.log(`Product quantity successfully updated.`);
						return res.send({status: true, message: `Product quantity successfully updated.`});
					};
				});

			}else{
				return res.send({status: false, message: "Product is not currently in cart. Cannot update."});
			};
		})
		//Catch any errors
		.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));
	}else{
		return res.send({status: false, message: `Quantity was not specified.`});
	};
};

//Delete the product in cart
module.exports.deleteCart = (req, res)=>{
	//Log a message to the console
	console.log(`Attempting to delete product in cart`);
	//Decode token
	const userData = auth.decode(req.headers.authorization);

	//Update User model
	User.findById(userData.id)
	.then(user=>{
		//Check if item is in cart
		const indexInCart = user.cart.findIndex(item=>item.productId === req.params.productId);
		if(indexInCart >= 0){
			user.cart.splice(indexInCart, 1);
			return user.save()
			.then((result, error)=>{
				if(error){
					return res.send({status: false, message: `An error occured during saving to database. ${error}`});
				}else{
					//Log a message to the console
					console.log(`Product successfully deleted.`);
					return res.send({status: true, message: `Product successfully removed from cart.`});
				};
			});

		}else{
			return res.send({status: false, message: "Product is not currently in cart. Cannot delete."});
		};
	})
	//Catch any errors
	.catch(error=>res.send({status: false, message: `An error occured during querying. ${error}`}));
};