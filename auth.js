//Initialize package dependencies
const jwt = require("jsonwebtoken");
const secret = "Capstone2Atienza";

//Export the Create Access Token Function
module.exports.createAccessToken = (userDetails) => {
	const data = {
		id: userDetails._id,
		email: userDetails.email,
		isAdmin: userDetails.isAdmin,
		firstName: userDetails.firstName,
        lastName: userDetails.lastName,
        mobileNo: userDetails.mobileNo,
        email: userDetails.email,
	};

	//Log a message to the console
	console.log("Created access token for:");
	console.log(data);
	return jwt.sign(data, secret, {});
};

//Export the Verify Function
module.exports.verify = (req, res, next)=>{
	let token = req.headers.authorization;
	if(typeof token === "undefined"){
		//Undefined Token means no token was sent to the API
		return res.send({status: false, message: "Failed. No token received."});
	}else{
		//Log a message to the console
		console.log(`Verifyting token: ${token}`);
		token = token.slice(7, token.length);
		//Validate the token using the "verify" method
		return jwt.verify(token, secret, (err, token)=>{
			if(err){
				res.send({status: false, message: `An error occured. ${err.message}`});
			}else{
				req.user = token;
				next();
			};
		});
	};
};

//Export the Verify User Type Function (receives a user type to check against and returns true if user is that type)
module.exports.verifyUserType = (type)=>{
	return (req, res, next)=>{
		//Log a message to the console
		console.log(`Verifying status of user: ${req.user}`);
		if(type =="admin"){
			console.log("check if admin")
			if(req.user.isAdmin){
				console.log(true)
				next();
			}else{
				console.log(false)
				return res.send(false);
			}
		}else{
			console.log("check if user")
			if(req.user.isAdmin){
				console.log(false)
				return res.send(false)
			}else{
				console.log(true)
				next();
			}
		}
	};
};

//Export the Decode Function
module.exports.decode = (token)=>{
	// Token recieved and is not undefined
	if(typeof token !== "undefined"){
		// Retrieves only the token and removes the "Bearer " prefix
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data)=>{
			if(err){
				return null;
			}else{
				// The "decode" method is used to obtain the information from the JWT
				// The "{complete:true}" option allows us to return additional information from the JWT token
				// Returns an object with access to the "payload" property which contains user information stored when the token was generated
				// The payload contains information provided in the "createAccessToken" method defined above (e.g. id, email and isAdmin)
				return jwt.decode(token,{complete:true}).payload;
			};
		});
	// Token does not exist
	}else{
		return null;
	};
};

//Export the Google Decode Function
module.exports.googleDecode = (token)=>{
	// Token recieved and is not undefined
	if(typeof token !== "undefined"){
		return jwt.decode(token,{complete:true}).payload;
	// Token does not exist
	}else{
		return null;
	};
};