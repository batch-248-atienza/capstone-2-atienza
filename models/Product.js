//Initialize package dependencies
const mongoose = require("mongoose");

//Define the user schema
const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product name is required"]
	},
	description: {
		type: String,
		required: [true, "Prodcut description is required"]
	},
	price: {
		type: Number,
		required: [true, "Product price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date, 
		default: new Date()
	},
	photo: {
		type: String,
		default: "data:image/png;base64,UklGRqAEAABXRUJQVlA4IJQEAACQPwCdASrCAZQBPlEoj0ajoqGhIjQoMHAKCWlu4XaeABnZ122KP8bdfdiOKReDJPegXNr/V2FQ0HyGPhAXNxw/q7U0+R/hCbm161za9a5tetc2vWubXrXNr1rm161za9a5tetc2vWubXrXNr1rm161za9a5tetc2vWubXrXNr1rm161za9a5tetc2vWubXrXNr1rm161za9a5tetc2vWubXrXNr1rm161za9a5tetc2vWubXrWSe6xQyck/2VhvCWOgOFa3amnyP8ITc1GEB1WJawmsZiBx2T2qwdNWDmX1I5ntTT5H+EJuR4n/jcTwJ6xDqwBwfJDl9v/dqafI/whNyPEVS/Rdas9KoJmm46NpjXEj7XWgTccDscFX9XamnyP8ISzncnHv8Qwu6RxMoDwlrgIxUe47wSi9rlgBFz4D1gpsq4Ag4f1dqafI/jY19EvRIGPzA3jRBGJUB6jh4MUtpskf4Qm5tetcjAZCzEhosG8JY0FKHMpiFef44f1dqafI/vg8PkLFxZq/amnyP8ITc2vWubXrXNr1rm161za9a5tetc2vWubXrXNr1rm161za9a5tetc2vWubXrXNr1rm161za9a5tetc2vWubXrXNr1rm161za9a5tetc2vWubXrXNr1rm161za9a5tetc2vWubXrXNr1rm161za9a5tetc3OAAAP78vgSyUAAAAAAABlblp1CFh3uDgPtdEn1auVzyOzhlURBitZQVbFHx8QqHclFMEU2wupYBQtiJnFqtL+jtk5HLYXATq0Nvp/5s5JBMfWSnCBJBx8CxkFlII9JwIkTp6YDIDd79tJyUPcRE+jHp5xpBC4PVFaJPbAZvD0VuZ8l1J6d/oVVpq+wRyDr+Guh8jUy8b20+NULJ2FB1sJiwp1A0GQnM8XYSv9HhAnB3Rww4QtEAtaoqD1XAThjLlBv7D9V3ddXkes2tjk9uBam4tzoT1k7EPzt6rQrqxEBYnhHyP4+cGtAVzxW/ak2uOvZNz9UsGYRxbicKEDPzHI2J+r4J1+q94xh6i43e/lzu1u3p6YkYClBWhWa3MYrqdOK2sLI0Th1CGsf9qrFFJ9RTMtZHUyGSf89EZM1NQmBouMo9UIGIch9cR8H3HLwh4Qrldu/EZuyKr4wv51f9Prb24uDV5G1jEfiP+xorSrrXW9ZOwKYJy/GYQRjxRV4quqYOReW5qxHv8TxRlJUIHPA9VtIpQ/8gFCV6cSU3LIW2WmJJ6gSOgPuX0XTBR9odrZ/Jf8TkwCfoCVLb0JO9oHn0VY9apquCoa0yHPe0EPAXbyRzdxS2rnX9L8iypcbWg8Pa0LsbAD2/G8SSVANvgy7Uqfu78s6IF5JDA1C1lMB1vqKA4IzraUIegrpogW7auNB7AALsm5+nSzJXfNuZtCbtThmoLOB8/jFv58sjTniy27PYpR2SmiJsQK8RplL318fqyUiUgs3cu28Y+rbxmuR/OQOHaB2XPxDtbkTbfGOwhbqQMM8u1HxYVI+FUA4KXKQEmd3Pi5SCxbC09Sn/iFh82hSSzCEAAAAAAAAAAAAAAA=="
	},
	orders: [
		{
			orderId: {
				type: String,
				required: [true, "Order ID is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			}
		}
	]
});

//Export the schema
module.exports = mongoose.model("Product", productSchema);