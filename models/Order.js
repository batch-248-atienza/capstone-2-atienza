//Initialize package dependencies
const mongoose = require("mongoose");

//Define the user schema
const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User ID is required"]
	},
	totalAmount: {
		type: Number,
		required: [true, "Total Amount is required"]
	},
	shippingFee: {
		type: Number,
		required: [true, "Shipping Fee is required"]
	},
	deliveryAddress: {
		type: String,
		required: [true, "Delivery Address is required"]
	},
	purchasedOn: {
		type: Date, 
		default: new Date()
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			},
			price: {
				type: Number,
				required: [true, "Price is required"]
			}
		}
	]
});

//Export the schema
module.exports = mongoose.model("Order", orderSchema);