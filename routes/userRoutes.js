//Initialize package dependencies
const express = require("express");
const router = express.Router();

//Initialize the controller and authenticator
const userController = require("../controllers/userControllers");
const auth = require("../auth");

//Declaring the functions needed from the authenticator
//Verify will check if there is a webtoken (user logged in)
//VerifyAdmin will check if the user is an administrator
const { verify, verifyUserType } = auth;

router.post("/checkEmailExists", userController.checkIfEmailExists);
router.post("/register", userController.registerUser);
router.post("/login", userController.loginUser);
router.post("/updateAdmin/:userId", verify, verifyUserType("admin"), userController.makeAdmin);
router.delete("/updateAdmin/:userId", verify, verifyUserType("admin"), userController.removeAdmin);
router.post("/checkout", verify, verifyUserType("user"), userController.createOrder);
router.get("/getUserDetails", verify, userController.getProfile);
router.get("/cart", verify, verifyUserType("user"), userController.getCart);
router.get("/", verify, verifyUserType("admin"), userController.returnAllUsers);

//Export the router
module.exports = router;