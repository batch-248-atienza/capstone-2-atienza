//Initialize package dependencies
const express = require("express");
const router = express.Router();

//Initialize the controller
const googleController = require("../controllers/googleController");

router.post("/", googleController.login);

//Export the router
module.exports = router;