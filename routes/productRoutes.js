//Initialize package dependencies
const express = require("express");
const router = express.Router();

//Initialize the controller and authenticator
const productController = require("../controllers/productControllers");
const auth = require("../auth");

//Declaring the functions needed from the authenticator
//Verify will check if there is a webtoken (user logged in)
//VerifyAdmin will check if the user is an administrator
const { verify, verifyUserType } = auth;

router.post("/", verify, verifyUserType("admin"), productController.createProduct);
router.get("/", productController.getAllActiveProducts);
router.get("/all", verify, verifyUserType("admin"), productController.getAllProducts);
router.get("/:productId", productController.getProduct);
router.put("/:productId", verify, verifyUserType("admin"), productController.updateProduct);
router.patch("/:productId/archive", verify, verifyUserType("admin"), productController.archiveProduct);
router.patch("/:productId/activate", verify, verifyUserType("admin"), productController.activateProduct);
router.post("/:productId/cart", verify, verifyUserType("user"), productController.addToCart);
router.patch("/:productId/cart", verify, verifyUserType("user"), productController.editCart);
router.delete("/:productId/cart", verify, verifyUserType("user"), productController.deleteCart);

//Export the router
module.exports = router;