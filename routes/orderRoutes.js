//Initialize package dependencies
const express = require("express");
const router = express.Router();

//Initialize the controller and authenticator
const orderController = require("../controllers/orderControllers");
const auth = require("../auth");

//Declaring the functions needed from the authenticator
//Verify will check if there is a webtoken (user logged in)
//VerifyAdmin will check if the user is an administrator
const { verify, verifyUserType } = auth;

router.get("/", verify, verifyUserType("admin"), orderController.returnAllOrders);
router.get("/getUserOrders", verify, verifyUserType("user"), orderController.getUserOrders);

//Export the router
module.exports = router;