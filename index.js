//Initialize package dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require("body-parser");

//Initialize varialbles
const DATABASE_USER = "admin";
const DATABASE_PASSWORD = "admin123";
const DATABASE_NAME = "capstone-2-atienza"

//Server Setup
const app = express();
const port = process.env.PORT || 4000;

//Import all the routes
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");
const googleRoute = require("./routes/googleRoute");

//Middlewares
app.use(cors());
app.use(bodyParser.json({ limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended:true}));

//Database Connection String
mongoose.set('strictQuery',true);
mongoose.connect(`mongodb+srv://${DATABASE_USER}:${DATABASE_PASSWORD}@clusterbatch248.ogkkhkk.mongodb.net/${DATABASE_NAME}?retryWrites=true&w=majority`,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

//Connect to the database
let db = mongoose.connection;
db.on("error",console.error.bind(console, "MongoDB Connection Error"));
db.once("open", ()=>console.log("Connected successfully to MongoDB"));

//Handle the routes of the API
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);
app.use("/google", googleRoute);

//Server listening
app.listen(port, ()=>{
		console.log(`API is now connected on port ${port}`);
});